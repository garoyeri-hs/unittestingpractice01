Work Process:
1) introduce a failing test (and watch it fail!)
2) write enough code to make the test pass (and only enough)
3) refactor
4) repeat

Requirement 1

Write a method greet(name) that interpolates name in
a simple greeting. For example, when name is "Bob",
the method should return a string "Hello, Bob.".


Requirement 2

Handle nulls by introducing a stand-in. For example,
when name is null, then the method should return the
string "Hello, my friend."

Requirement 3

Handle shouting. When name is all uppercase, then the
method should shout back to the user. For example, when
name is "JERRY" then the method should return the string
"HELLO JERRY!"

Requirement 4

Handle two names of input. When name is an array of two
names (or, in languages that support it, varargs or a
splat), then both names should be printed. For example,
when name is ["Jill", "Jane"], then the method should
return the string "Hello, Jill and Jane."

Requirement 4.5

For the two names of input, make sure that the requirements
1-2 are also followed.
