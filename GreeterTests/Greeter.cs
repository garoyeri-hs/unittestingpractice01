﻿namespace GreeterTests
{
    using System;
    using System.Runtime.InteropServices.WindowsRuntime;

    internal class Greeter
    {
        public Greeter()
        {
        }

        public string Greet(string name)
        {
            if (name!=null && name.ToUpper()==name)
            {
                return $"HELLO {name}!";
            }
            return $"Hello, {(string.IsNullOrEmpty(name) ? "my friend" : name)}.";
        }

        public string Greet(string name1, string name2)
        {
            return $"Hello, {HandleName(name1)} and {HandleName(name2)}.";
        }

        private string HandleName(string name)
        {
            return (string.IsNullOrEmpty(name) ? "my friend" : name);
        }
    }
}