﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GreeterTests
{
    using Shouldly;

    public class GreeterTests
    {
        public void ShouldGreetBob()
        {
            var greeter = new Greeter();
            greeter.Greet("Bob").ShouldBe("Hello, Bob.");
        }

        public void ShouldGreetArmando()
        {
            var greeter = new Greeter();
            greeter.Greet("Armando").ShouldBe("Hello, Armando.");
        }

        public void ShouldSayHelloMyFriendOnNull()
        {
            var greeter = new Greeter();
            greeter.Greet(null).ShouldBe("Hello, my friend.");
        }

        public void ShouldShoutJerry()
        {
            var greeter = new Greeter();
            greeter.Greet("JERRY").ShouldBe("HELLO JERRY!");
        }

        public void ShouldGreetTwoPeople()
        {
            var greeter = new Greeter();
            greeter.Greet("Jerry", "Jane").ShouldBe("Hello, Jane and Jane.");
        }

        public void ShouldGreetTwoNullPeopleAsFriends()
        {
            var greeter = new Greeter();
            greeter.Greet(null, null)
                .ShouldBe("Hello, my friend and my friend.");
        }
    }
}
